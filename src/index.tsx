import { NativeModules } from 'react-native';

type DemoLibraryType = {
  multiply(a: number, b: number): Promise<number>;
};

const { DemoLibrary } = NativeModules;

export default DemoLibrary as DemoLibraryType;
